class Velodrome():
    def __init__(self, name: str, length: float) -> None:
        """
        A bicyle race track
        
        Arguments:
            name {str} -- The track name
            length {float} -- The track length
        """

        self.name = name
        self.length = length
    