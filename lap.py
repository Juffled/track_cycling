from rider import Rider

class Lap():
    def __init__(self, lap_id: int, primary_time: float, rider: Rider) -> None:
        """
        A single lap object
        
        Arguments:
            lap_id {int} -- The lap number
            primary_time {float} -- The lap primary time
            rider {Rider} -- The Rider riding the lap
        """
        self.lap_id = lap_id
        self.primary_time = primary_time
        self.rider = rider
        self.added_times = []

    def add_time(self, added_time: float) -> None:
        """
        Add additional time to the lap e.g. changeover time
        
        Arguments:
            added_time {float} -- The time to add to the lap [seconds]
         """
        self.added_times.append(added_time)

    @property
    def t(self) -> float:
        """       
        Returns:
            float -- The total lap time
        """

        t1 = self.primary_time
        for added_time in self.added_times:
            t1 += added_time
        
        return t1