from race import Race

class PostProcessor():
    def __init__(self, race: Race) -> None:
        self.race = race
        self.total_laps = len(race.laps)
        self.total_dist = self.total_laps * self.race.track.length

    def rider_summary(self) -> None:
        for rider in self.race.riders:
            rider_dist = rider.total_laps * self.race.track.length
            print(str(rider.name) + ": " + str(rider.total_laps) + " laps - " + str(round(rider_dist,2)) + "m or " + str(round(rider_dist / 1000, 2)) + "km or " + str(round(rider_dist / 1609.34, 2)) + "miles")

    def team_summary(self) -> None:
        print("Team Total: " + str(len(self.race.laps)) + " laps - " + str(round(self.total_dist,2)) + "m or " + str(round(self.total_dist / 1000, 2)) + "km or " + str(round(self.total_dist / 1609.34, 2)) + "miles")

    def lap_times(self) -> None:
        for lap in self.race.laps:
            print("Lap " + str(lap.lap_id) + ": " + str(round(lap.t, 2)) + "secs - " + str(lap.rider.name))

