from track import Velodrome
from rider import Rider
from race import Race
from changeover import changeover_laps
from post_processor import PostProcessor

derby = Velodrome(name="Derby", length=250)
rhys = Rider(name='Rhys', mph=20)
tom = Rider(name='Tom', mph=20)
david = Rider(name='David', mph=20)
chris = Rider(name='Chris', mph=20)
robin = Rider(name='Robin', mph=20)
george = Rider(name='George', mph=20)
riders=[rhys, tom, david, chris, robin, george]

# A single race
# cyclone = Race(riders=riders, time_hrs=24, session_length_mins=20, track=derby, t_swap=10.5)

# summary = PostProcessor(race=cyclone)
# summary.lap_times()
# summary.team_summary()
# summary.rider_summary()

# A range of races
all_data = []
for i in range(15,60,10):
    race = Race(riders=riders, time_hrs=24, session_length_mins=i, track=derby, t_swap=10.5)
    summary = PostProcessor(race=race)
    # summary.team_summary()
    all_data.append([i, len(race.laps)])

# print(max([race_data[-1] for race_data in all_data]))