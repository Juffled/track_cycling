from lap import Lap
from rider import Rider
from track import Velodrome
from changeover import changeover_laps

class Race():
    def __init__(self, riders: list, time_hrs: float, session_length_mins: int, track: Velodrome, t_swap: float) -> None:
        """
        A track cycle race
        
        Arguments:
            riders {list} -- A list of Rider objects in the order they are going to ride
            time_hrs {float} -- The duration of the race [hrs]
            session_length_mins {int} -- The time of each Riders session/stint [mins]
            track {Velodrome} -- The Velodrome the race is taking place at
            t_swap {float} -- The time it takes for a changeover when static [s]
        """
        # Race Settings
        self.riders = riders
        self.track = track
        self.t_swap = t_swap
        self.race_length_secs = time_hrs * 60 * 60
        self.session_length_secs = session_length_mins * 60
        
        # Riders
        self._active_rider: Rider = None
        self._next_rider: Rider = None

        self._lap_num: int = None
        self.laps = []
        
        self._start()
        self._end()

    def changeover(self) -> None:
        """
        Switches riders and reorders the list
        """
        self.riders = self.riders[1:] + [self.riders[0]]
        self._active_rider = self.riders[0]
        self._next_rider = self.riders[1]

    
    def _start(self) -> None:
        """
        Start the race!
        """
        # 1st lap and set the riders
        self._lap_num = 1
        self._active_rider = self.riders[0]
        self._next_rider = self.riders[1]

        # Calculate the added time to get upto speed
        _, accel_time = changeover_laps(rider1=self._active_rider, rider2=self._active_rider, track=self.track, t_swap=0.)

        # While the accumulated time is less than the race length, we ride!
        while self.t < self.race_length_secs:
            # If the active riders session time is less than the defined session time he rides!
            if self._active_rider.session_time < self.session_length_secs:
                self._active_rider.lap(track=self.track)
                self.laps.append(Lap(lap_id=self._lap_num, rider=self._active_rider, 
                                     primary_time=self._active_rider.laptime(track=self.track)))
                
                # Add any added times to the lap, used for initial start and changeover accelerations
                if accel_time is not None:
                    self.laps[-1].add_time(added_time = accel_time)
                
                accel_time = None

                self._lap_num += 1

            # If the active riders time comes to end he does not start a new lap and hands over to the next rider
            elif self._active_rider.session_time > self.session_length_secs:                
                # Calculate changeover time
                t_added_in, t_added_out = changeover_laps(rider1=self._active_rider, rider2=self._next_rider, 
                                                          track=self.track, t_swap=self.t_swap)
                # Add deceleration time and static time to the current lap
                self.laps[-1].add_time(added_time = t_added_in)

                # Add acceleration time to the next lap
                accel_time = t_added_out

                # Reset the active riders session stats
                self._active_rider.pit()

                # Change active riders
                self.changeover()

                self._lap_num += 1

    def _end(self) -> None:
        """
        The race ends
        """
        for rider in self.riders:
            rider.rest()

    @property
    def t(self) -> float:
        """
        Returns:
            float -- The current race duration
        """
        race_time = 0.

        for lap in self.laps:
            race_time += lap.t
        
        return race_time