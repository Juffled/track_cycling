from track import Velodrome

class Rider():
    def __init__(self, name: str, mph: float, decel: float =0.231, accel: float =0.4572):
        """
        A bicycle riding superstar

        Sources:  Deceleration rate: genral studies use 0.231 m/s^2 in cities and 
                  https://www.diva-portal.org/smash/get/diva2:795377/FULLTEXT01.pdf

                  Acceleration rate: https://www.pdx.edu/ibpi/sites/www.pdx.edu.ibpi/files/Bicycle%20Performance%20Forthcomming%202013.pdf
        
        Arguments:
            name {str} -- riders name
            mph {float} -- average speed [mph]
        
        Keyword Arguments:
            decel {float} -- acceleration rate (default: {0.231})
            accel {float} -- deceleration rate (default: {0.4572})
        """
        self.name = name
        self.mph = mph
        self.accel = accel
        self.decel = decel
        self.session_lap = 0
        self.session_time = 0.
        self.num_sessions = 0
        self.total_laps = 0

    def rest(self) -> None:
        """
        Reset the riders sessions and laps, i.e. reset / rest
        """
        self.num_sessions = 0
        self.total_laps = 0

    def lap(self, track: Velodrome) -> None:
        """
        The riding superstar completes a lap of the track
        
        Arguments:
            track {Velodrome} -- The track the rider is on
        """
        self.session_lap += 1
        self.total_laps += 1
        self.session_time += self.laptime(track=track)
    
    def pit(self) -> None:
        """
        The bicycle superstar has destroyed the track and lets someone else have a go
        """
        self.session_lap = 0
        self.session_time = 0.
        self.num_sessions += 1

    def laptime(self, track: Velodrome) -> float:
        """
        The time of the superstars lap
        
        Arguments:
            track {Velodrome} -- The track he is riding on
        
        Returns:
            float -- A laptime
        """
        # TODO: develop this properly
        speed = self.speed - (self.num_sessions + 1) * 0.01 * self.session_lap
        return track.length / speed

    @property
    def speed(self) -> float:
        return self.mph * 0.44704