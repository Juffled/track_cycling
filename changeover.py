from rider import Rider
from track import Velodrome

def changeover_laps(rider1: Rider, rider2: Rider, track: Velodrome, t_swap: float) -> tuple:
    """
    Calculates changeover times for end of rider sessions
    
    Arguments:
        rider1 {Rider} -- The in lap Rider
        rider2 {Rider} -- The out lap Rider
        track {Velodrome} -- The track the changeover is happening at
        t_swap {float} -- The stationary time of the changeover [seconds]
    
    Returns:
        {float} -- The additional time on the in lap
        {float} -- The additional time on the out lap
    """
    # In lap
    t_decel = rider1.speed / rider1.decel
    s_decel = rider1.speed * t_decel - 0.5 * rider1.decel * t_decel**2
    t_in = (track.length - s_decel) / rider1.speed
    t_in_normal = track.length / rider1.speed
    t_in_added_time = t_decel + t_in - t_in_normal 

    # Out lap
    t_accel = rider2.speed / rider1.accel
    s_accel = 0.5 * rider2.accel * t_accel**2
    t_out = (track.length - s_accel) / rider2.speed
    t_out_normal = track.length / rider2.speed
    t_out_added_time = t_accel + t_out - t_out_normal

    # Dont forget the static time
    return t_in_added_time + t_swap, t_out_added_time
